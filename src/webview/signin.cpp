#include "signin.h"
#include <QtWidgets>
#include <QtNetwork>
#include <QWebEngineView>
#include <QWebEnginePage> 
#include <QWebEngineHttpRequest> 
#include <QWebEngineProfile>
#include <QFileSystemWatcher> 
#include <QDesktopServices>

SignIn::SignIn(){}
SignIn::~SignIn(){}

void SignIn::openWebView(QString urlString){
        QByteArray headers[] = {
                QByteArray::fromStdString("USER_AGENT"),
                QByteArray::fromStdString("OCS-APIREQUEST")
        };
        QByteArray headerValues[] = {
                QByteArray::fromStdString("Mozilla/5.0 sharefiles"),
                QByteArray::fromStdString("true")
        };
        qDebug()<<urlString;
        request.setUrl(QUrl("https://" + urlString + "/index.php/login/flow"));
        request.setHeader(headers[0], headerValues[0]);
        request.setHeader(headers[1], headerValues[1]);
        view->load(request);

        //delete cookies
        view->page()->profile()->setPersistentCookiesPolicy(QWebEngineProfile::NoPersistentCookies);
        QDesktopServices::setUrlHandler("nc", this, "finalUrlHandler");

        view->show();
        view->resize(424, 650);
}

void SignIn::finalUrlHandler(const QUrl &url){
        QUrl final_url = url;
        qDebug()<<"URL"<<final_url;
        view->close();
        delete view;
        isSigninDone = true;
}