import QtQuick 2.1
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4

import QtWebEngine 1.0

Kirigami.ApplicationWindow {
    id: root

    title: "Sharefiles"

    width: 360
    height: 600

    pageStack.initialPage: mainPageComponent

    Component {
        id: mainPageComponent

        Kirigami.Page {
            id: pane
            title: "Sharefiles"

            Rectangle{

                color: "white"
                anchors.fill: parent
                ColumnLayout {
                    spacing: 10
                    anchors.fill: parent
                
                    Item { Layout.fillHeight: true } 

                    TextField{
                        id: textfield
                        placeholderText: qsTr("Server address https://...")
                        width: 200    
                        Layout.alignment: Qt.AlignCenter               
                    }

                    ToolButton{
                        icon.name: "plasma-browser-integration"
                        icon.color:"black"
                        icon.width:120
                        icon.height:100
                        text:"login"                  
                        display: ToolButton.TextUnderIcon
                        onClicked:{
                            console.log(textfield.text);
                            signin.openWebView(textfield.text);
                        }
                        Layout.alignment: Qt.AlignCenter
                    }
                
                    Item { Layout.fillHeight: true } 
                }
            }
        }
    }
}